#pragma once

#include <iostream>

using namespace std;

class TicTacToe
{
private:

	// Private fields
	char m_playerTurn;
	bool m_playerWon;
	int m_numTurns;
	bool m_xAry[9];
	bool m_oAry[9];

	// Private methods
	char GetSquare(const int position) const
	{
		return (m_xAry[position - 1]) ? 'x' : (m_oAry[position - 1]) ? 'o' : ' ';
	}

	bool HasWin(const bool* mvAry) const
	{
		return ((mvAry[0] && mvAry[1] && mvAry[2]) || // Begin horizontals
				(mvAry[3] && mvAry[4] && mvAry[5]) ||
				(mvAry[6] && mvAry[7] && mvAry[8]) ||
				(mvAry[0] && mvAry[3] && mvAry[6]) || // Begin verticals
				(mvAry[1] && mvAry[4] && mvAry[7]) ||
				(mvAry[2] && mvAry[5] && mvAry[8]) ||
				(mvAry[0] && mvAry[4] && mvAry[8]) || // Begin diagonals
				(mvAry[2] && mvAry[4] && mvAry[6]));
	}

public:

	// Constructor - initialize fields
	TicTacToe()
	{
		m_playerTurn = 'X';
		m_playerWon = false;
		m_numTurns = 0;
		for (int i = 0; i < 9; i++)
		{
			m_xAry[i] = false;
			m_oAry[i] = false;
		}
	}

	// Getters
	char GetPlayerTurn() const { return m_playerTurn; }

	// Other public methods
	void DisplayBoard() const
	{
		cout << " " << GetSquare(1) << " | " << GetSquare(2) << " | " << GetSquare(3) << "\n";
		cout << "---|---|---\n";
		cout << " " << GetSquare(4) << " | " << GetSquare(5) << " | " << GetSquare(6) << "\n";
		cout << "---|---|---\n";
		cout << " " << GetSquare(7) << " | " << GetSquare(8) << " | " << GetSquare(9) << "\n\n";
	}
	
	bool IsValidMove(const int position) const
	{
		return !(position < 1 || position > 9 || m_xAry[position - 1] || m_oAry[position - 1]);
	}

	void Move(const int position)
	{
		m_xAry[position - 1] = (m_playerTurn == 'X'); // Already checked whether move was valid, so this
		m_oAry[position - 1] = (m_playerTurn == 'O'); // won't overwrite a previous move
		m_numTurns++;
		m_playerWon = HasWin(m_xAry) || HasWin(m_oAry);
		if (!IsOver()) m_playerTurn = (m_playerTurn == 'X') ? 'O' : 'X';
	}

	bool IsOver() const { return m_playerWon || m_numTurns == 9; }

	void DisplayResult() const
	{
		if (m_playerWon) cout << "Player " << GetPlayerTurn() << " is the winner!\n";
		else cout << "Cat! Players tied!\n";
	}
};